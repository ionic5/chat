// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCL9TYoIYhgqh040TSJV8CSKO7HdTepVXc",
    authDomain: "chat-8bde1.firebaseapp.com",
    databaseURL: "https://chat-8bde1.firebaseio.com",
    projectId: "chat-8bde1",
    storageBucket: "chat-8bde1.appspot.com",
    messagingSenderId: "1000909049628",
    appId: "1:1000909049628:web:fa6f1e8d338c4ede6c34d9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
