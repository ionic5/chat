import { Component } from '@angular/core';
import { ChatService } from '../chat.service';
import { Message } from '../Message';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  messages = [];
  messageText: string;

  constructor(private chatService: ChatService) {}

  ngOnInit() {
    let messagesRes = this.chatService.getMessages();

    messagesRes.snapshotChanges().subscribe(res => {
      this.messages = [];
      res.forEach(item => {
        let msg = item.payload.toJSON();
        msg["$key"] = item.key;
        this.messages.push(msg as Message);
      })
    })
  }

  sendMessage(event) {
    if (event.keyCode === 13) {
        const message: Message = new Message();
        message.text = this.messageText;
        this.chatService.createMessage(message).then(res => {
          //console.log(res);
          this.messageText = '';
        }). catch (error => console.log(error));
    }
  }

}
