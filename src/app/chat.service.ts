import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Message } from './Message';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  messageListRef: AngularFireList<any>;
  messageRef: AngularFireObject<any>;

  constructor(private db: AngularFireDatabase) { }

  createMessage(message: Message) {
    return this.messageListRef.push({
      text: message.text
    })
  }

  getMessages() {
    this.messageListRef = this.db.list('message');
    return this.messageListRef;
  }
}
